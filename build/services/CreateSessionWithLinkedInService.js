"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);
var __async = (__this, __arguments, generator) => {
  return new Promise((resolve, reject) => {
    var fulfilled = (value) => {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    };
    var rejected = (value) => {
      try {
        step(generator.throw(value));
      } catch (e) {
        reject(e);
      }
    };
    var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
    step((generator = generator.apply(__this, __arguments)).next());
  });
};

// src/services/CreateSessionWithLinkedInService.ts
var CreateSessionWithLinkedInService_exports = {};
__export(CreateSessionWithLinkedInService_exports, {
  CreateSessionWithLinkedInService: () => CreateSessionWithLinkedInService
});
module.exports = __toCommonJS(CreateSessionWithLinkedInService_exports);
var import_axios = __toESM(require("axios"));
var import_fs = __toESM(require("fs"));
var import_jsonwebtoken = require("jsonwebtoken");
var import_node_crypto = __toESM(require("crypto"));
var import_path = __toESM(require("path"));

// src/errors/AppError.ts
var AppError = class {
  constructor(message, statusCode = 400) {
    this.message = message;
    this.statusCode = statusCode;
  }
};

// src/prisma/index.ts
var import_client = require("@prisma/client");
var prisma = new import_client.PrismaClient();

// src/services/CreateSessionWithLinkedInService.ts
var uploadPath = import_path.default.resolve(__dirname, "..", "temp");
var CreateSessionWithLinkedInService = class {
  execute(linkedin_code) {
    return __async(this, null, function* () {
      var _a, _b;
      try {
        const tokenResponse = yield import_axios.default.post(
          `https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=${linkedin_code}&redirect_uri=${process.env.LINKEDIN_REDIRECT_URI}&client_id=${process.env.LINKEDIN_CLIENT}&client_secret=${process.env.LINKEDIN_SECRET}`
        );
        const profileResponse = yield import_axios.default.get(
          `https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~digitalmediaAsset:playableStreams))`,
          {
            headers: {
              Authorization: `Bearer ${tokenResponse.data.access_token}`
            }
          }
        );
        const { id, firstName, lastName } = profileResponse.data;
        const name = `${Object.values(firstName.localized)[0]} ${Object.values(lastName.localized)[0]}`;
        let picture = (_b = (_a = profileResponse.data.profilePicture["displayImage~"].elements.pop()) == null ? void 0 : _a.identifiers.pop()) == null ? void 0 : _b.identifier;
        let user;
        user = yield prisma.user.findUnique({
          where: { token: id }
        });
        if (!user) {
          if (picture) {
            try {
              const resposta = yield import_axios.default.get(picture, {
                responseType: "arraybuffer"
              });
              const buffer = Buffer.from(resposta.data, "binary");
              const fileName = `${import_node_crypto.default.randomUUID()}.jpeg`;
              import_fs.default.writeFileSync(`${uploadPath}/${fileName}`, buffer);
              picture = `${process.env.API_URL}/files/${fileName}`;
            } catch (erro) {
              console.error("Ocorreu um erro ao baixar a imagem:", erro);
            }
          }
          user = yield prisma.user.create({
            data: {
              name,
              picture,
              token: id
            }
          });
        }
        const token = (0, import_jsonwebtoken.sign)({}, `${process.env.JWT_SECRET}`, {
          subject: user.id,
          expiresIn: "1d"
        });
        return { user, token };
      } catch (err) {
        console.log(err);
        throw new AppError("Ocorreu um erro ao se autenticar com LinkedIn.");
      }
    });
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  CreateSessionWithLinkedInService
});
