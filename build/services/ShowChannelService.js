"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);
var __async = (__this, __arguments, generator) => {
  return new Promise((resolve, reject) => {
    var fulfilled = (value) => {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    };
    var rejected = (value) => {
      try {
        step(generator.throw(value));
      } catch (e) {
        reject(e);
      }
    };
    var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
    step((generator = generator.apply(__this, __arguments)).next());
  });
};

// src/services/ShowChannelService.ts
var ShowChannelService_exports = {};
__export(ShowChannelService_exports, {
  ShowChannelService: () => ShowChannelService
});
module.exports = __toCommonJS(ShowChannelService_exports);

// src/prisma/index.ts
var import_client = require("@prisma/client");
var prisma = new import_client.PrismaClient();

// src/services/ShowChannelService.ts
var ShowChannelService = class {
  execute(_0) {
    return __async(this, arguments, function* ({ channel_id, user_id }) {
      const channelUser = yield prisma.channelUser.findUnique({
        where: {
          user_id
        }
      });
      if (channelUser) {
        throw new Error("User already in a channel");
      }
      const channel = yield prisma.channel.findUnique({
        where: { id: channel_id }
      });
      if (!channel) {
        throw new Error("Channel not found");
      }
      const customers = yield prisma.channelUser.findMany({
        where: {
          channel_id,
          user_id: {
            notIn: user_id
          }
        }
      });
      if (customers.length > 2) {
        throw new Error("Channel is full");
      }
    });
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  ShowChannelService
});
