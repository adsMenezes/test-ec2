"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __async = (__this, __arguments, generator) => {
  return new Promise((resolve, reject) => {
    var fulfilled = (value) => {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    };
    var rejected = (value) => {
      try {
        step(generator.throw(value));
      } catch (e) {
        reject(e);
      }
    };
    var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
    step((generator = generator.apply(__this, __arguments)).next());
  });
};

// src/server.ts
var import_config = require("dotenv/config");
var import_cors = __toESM(require("cors"));
var import_express2 = __toESM(require("express"));
var import_http = __toESM(require("http"));
var import_path2 = __toESM(require("path"));
var import_jsonwebtoken3 = require("jsonwebtoken");
var import_socket = require("socket.io");

// src/errors/AppError.ts
var AppError = class {
  constructor(message, statusCode = 400) {
    this.message = message;
    this.statusCode = statusCode;
  }
};

// src/middleware/exceptions.ts
function exceptions(err, request, response, _) {
  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      status: "Error",
      message: err.message
    });
  }
  if (err instanceof SyntaxError) {
    return response.status(400).json({
      status: "Error",
      message: "Bad Request!"
    });
  }
  console.error(err);
  return response.status(500).json({
    status: "Error",
    message: "Internal server error"
  });
}

// src/prisma/index.ts
var import_client = require("@prisma/client");
var prisma = new import_client.PrismaClient();

// src/routes/index.ts
var import_express = require("express");

// src/middleware/ensureAuthenticated.ts
var import_jsonwebtoken = require("jsonwebtoken");
function ensureAuthenticated(request, response, next) {
  const { authorization } = request.headers;
  if (!authorization) {
    return response.status(401).json({
      error: "token.invalid"
    });
  }
  const [, token] = authorization.split(" ");
  try {
    const { sub } = (0, import_jsonwebtoken.verify)(token, `${process.env.JWT_SECRET}`);
    request.user_id = sub;
    return next();
  } catch (e) {
    return response.status(401).json({
      error: "token.expired"
    });
  }
}

// src/services/CreateSessionWithLinkedInService.ts
var import_axios = __toESM(require("axios"));
var import_fs = __toESM(require("fs"));
var import_jsonwebtoken2 = require("jsonwebtoken");
var import_node_crypto = __toESM(require("crypto"));
var import_path = __toESM(require("path"));
var uploadPath = import_path.default.resolve(__dirname, "..", "temp");
var CreateSessionWithLinkedInService = class {
  execute(linkedin_code) {
    return __async(this, null, function* () {
      var _a, _b;
      try {
        const tokenResponse = yield import_axios.default.post(
          `https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=${linkedin_code}&redirect_uri=${process.env.LINKEDIN_REDIRECT_URI}&client_id=${process.env.LINKEDIN_CLIENT}&client_secret=${process.env.LINKEDIN_SECRET}`
        );
        const profileResponse = yield import_axios.default.get(
          `https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~digitalmediaAsset:playableStreams))`,
          {
            headers: {
              Authorization: `Bearer ${tokenResponse.data.access_token}`
            }
          }
        );
        const { id, firstName, lastName } = profileResponse.data;
        const name = `${Object.values(firstName.localized)[0]} ${Object.values(lastName.localized)[0]}`;
        let picture = (_b = (_a = profileResponse.data.profilePicture["displayImage~"].elements.pop()) == null ? void 0 : _a.identifiers.pop()) == null ? void 0 : _b.identifier;
        let user;
        user = yield prisma.user.findUnique({
          where: { token: id }
        });
        if (!user) {
          if (picture) {
            try {
              const resposta = yield import_axios.default.get(picture, {
                responseType: "arraybuffer"
              });
              const buffer = Buffer.from(resposta.data, "binary");
              const fileName = `${import_node_crypto.default.randomUUID()}.jpeg`;
              import_fs.default.writeFileSync(`${uploadPath}/${fileName}`, buffer);
              picture = `${process.env.API_URL}/files/${fileName}`;
            } catch (erro) {
              console.error("Ocorreu um erro ao baixar a imagem:", erro);
            }
          }
          user = yield prisma.user.create({
            data: {
              name,
              picture,
              token: id
            }
          });
        }
        const token = (0, import_jsonwebtoken2.sign)({}, `${process.env.JWT_SECRET}`, {
          subject: user.id,
          expiresIn: "1d"
        });
        return { user, token };
      } catch (err) {
        console.log(err);
        throw new AppError("Ocorreu um erro ao se autenticar com LinkedIn.");
      }
    });
  }
};

// src/controllers/CreateSessionWithLinkedInController.ts
var CreateSessionWithLinkedInController = class {
  handler(request, response) {
    return __async(this, null, function* () {
      const { code } = request.body;
      const createSessionWithLinkedIn = new CreateSessionWithLinkedInService();
      const { user, token } = yield createSessionWithLinkedIn.execute(code);
      return response.json({ user, token });
    });
  }
};

// src/services/CreateChannelService.ts
var CreateChannelService = class {
  execute() {
    return __async(this, null, function* () {
      const channel = yield prisma.channel.create({
        data: {}
      });
      return channel;
    });
  }
};

// src/controllers/CreateChannelController.ts
var CreateChannelController = class {
  handler(request, response) {
    return __async(this, null, function* () {
      const createChannel = new CreateChannelService();
      const channel = yield createChannel.execute();
      return response.json(channel);
    });
  }
};

// src/services/ProfileUserService.ts
var ProfileUserService = class {
  execute(user_id) {
    return __async(this, null, function* () {
      const user = yield prisma.user.findFirst({
        where: {
          id: user_id
        }
      });
      if (!user) {
        throw new AppError("User does not exist.");
      }
      return user;
    });
  }
};

// src/controllers/ProfileUserController.ts
var ProfileUserController = class {
  handler(request, response) {
    return __async(this, null, function* () {
      const { user_id } = request;
      const service = new ProfileUserService();
      const result = yield service.execute(user_id);
      return response.json(result);
    });
  }
};

// src/services/ShowChannelService.ts
var ShowChannelService = class {
  execute(_0) {
    return __async(this, arguments, function* ({ channel_id, user_id }) {
      const channelUser = yield prisma.channelUser.findUnique({
        where: {
          user_id
        }
      });
      if (channelUser) {
        throw new Error("User already in a channel");
      }
      const channel = yield prisma.channel.findUnique({
        where: { id: channel_id }
      });
      if (!channel) {
        throw new Error("Channel not found");
      }
      const customers = yield prisma.channelUser.findMany({
        where: {
          channel_id,
          user_id: {
            notIn: user_id
          }
        }
      });
      if (customers.length > 2) {
        throw new Error("Channel is full");
      }
    });
  }
};

// src/controllers/ShowChannelController.ts
var ShowChannelController = class {
  handler(request, response) {
    return __async(this, null, function* () {
      const { user_id } = request;
      const { channel_id } = request.params;
      const showChannel = new ShowChannelService();
      yield showChannel.execute({
        channel_id,
        user_id
      });
      return response.send();
    });
  }
};

// src/routes/index.ts
var routes = (0, import_express.Router)();
var createSessionWithLinkedInController = new CreateSessionWithLinkedInController();
var profileUserController = new ProfileUserController();
var createChannelController = new CreateChannelController();
var showChannelController = new ShowChannelController();
routes.get("/", (request, response) => {
  return response.json({
    message: "Seja melhor que ontem!",
    author: "Anderson Menezes <anderson@codrop.com.br>"
  });
});
routes.post("/sessions/linkedin", createSessionWithLinkedInController.handler);
routes.get("/me", ensureAuthenticated, profileUserController.handler);
routes.get("/channels/:channel_id", ensureAuthenticated, showChannelController.handler);
routes.post("/channels", ensureAuthenticated, createChannelController.handler);

// src/server.ts
var app = (0, import_express2.default)();
var server = import_http.default.createServer(app);
app.use((0, import_cors.default)());
app.use("/files", import_express2.default.static(import_path2.default.resolve(__dirname, "..", "temp")));
app.use(import_express2.default.json());
app.use(routes);
app.use(exceptions);
var io = new import_socket.Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});
io.use((socket, next) => {
  const { token } = socket.handshake.auth;
  try {
    const { sub } = (0, import_jsonwebtoken3.verify)(token, `${process.env.JWT_SECRET}`);
    socket.user_id = sub;
    next();
  } catch (e) {
    next(new Error("invalid"));
  }
});
io.on("connection", (socket) => {
  console.log("connected");
  socket.on("join", (_0) => __async(exports, [_0], function* ({ channel_id }) {
    const channel = yield prisma.channel.findUnique({
      where: {
        id: channel_id
      }
    });
    if (!channel) {
      return;
    }
    yield prisma.channelUser.create({
      data: {
        channel_id,
        user_id: socket.user_id,
        socket_id: socket.id
      }
    });
    const customers = yield prisma.channelUser.findMany({
      where: {
        channel_id
      },
      include: {
        user: true
      }
    });
    socket.join(channel_id);
    io.to(channel_id).emit("channel.set.customers", customers);
    if (customers.length >= 2) {
      io.in(socket.id).emit("peer.get.session.offer");
    }
    console.log("join -", channel_id);
  }));
  socket.on("leave", () => __async(exports, null, function* () {
    const channel_id = Array.from(socket.rooms)[1];
    yield prisma.channelUser.delete({
      where: {
        user_id: socket.user_id
      }
    });
    const customers = yield prisma.channelUser.findMany({
      where: {
        channel_id
      },
      include: {
        user: true
      }
    });
    if (customers.length <= 0) {
      yield prisma.channel.delete({
        where: {
          id: channel_id
        }
      });
      return;
    }
    socket.leave(channel_id);
    io.to(channel_id).emit("channel.set.customers", customers);
    io.to(channel_id).emit("peer.reset");
    console.log("leave -", channel_id);
  }));
  socket.on("channel.isMicMuted", (isMicMuted) => __async(exports, null, function* () {
    const channel_id = Array.from(socket.rooms)[1];
    yield prisma.channelUser.update({
      data: {
        is_microphone_active: isMicMuted
      },
      where: {
        user_id: socket.user_id
      }
    });
    const customers = yield prisma.channelUser.findMany({
      where: {
        channel_id
      },
      include: {
        user: true
      }
    });
    io.to(channel_id).emit("channel.set.customers", customers);
  }));
  socket.on("channel.session", (session) => __async(exports, null, function* () {
    const channel_id = Array.from(socket.rooms)[1];
    io.to(channel_id).except(socket.id).emit("peer.set.remote", session);
  }));
  socket.on("channel.candidate", (candidate) => __async(exports, null, function* () {
    const channel_id = Array.from(socket.rooms)[1];
    io.to(channel_id).except(socket.id).emit("peer.set.candidate", candidate);
  }));
  socket.on("talking", (status) => __async(exports, null, function* () {
    const channel_id = Array.from(socket.rooms)[1];
    io.to(channel_id).except(socket.id).emit("talking", status);
  }));
  socket.on("disconnect", () => __async(exports, null, function* () {
    const channelUser = yield prisma.channelUser.findUnique({
      where: {
        user_id: socket.user_id
      },
      include: {
        channel: true
      }
    });
    if (!channelUser) {
      console.log("disconnect");
      return;
    }
    yield prisma.channelUser.delete({
      where: { user_id: socket.user_id }
    });
    const customers = yield prisma.channelUser.findMany({
      where: {
        channel_id: channelUser.channel.id
      },
      include: {
        user: true
      }
    });
    if (customers.length <= 0) {
      yield prisma.channel.delete({
        where: {
          id: channelUser.channel.id
        }
      });
      return;
    }
    io.to(channelUser.channel.id).emit("channel.set.customers", customers);
    io.to(channelUser.channel.id).emit("peer.reset");
    console.log("channel disconnect -", channelUser.channel.id);
  }));
});
server.listen(80, () => {
  console.log("listening on port 80");
});
