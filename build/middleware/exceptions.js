"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/middleware/exceptions.ts
var exceptions_exports = {};
__export(exceptions_exports, {
  exceptions: () => exceptions
});
module.exports = __toCommonJS(exceptions_exports);

// src/errors/AppError.ts
var AppError = class {
  constructor(message, statusCode = 400) {
    this.message = message;
    this.statusCode = statusCode;
  }
};

// src/middleware/exceptions.ts
function exceptions(err, request, response, _) {
  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      status: "Error",
      message: err.message
    });
  }
  if (err instanceof SyntaxError) {
    return response.status(400).json({
      status: "Error",
      message: "Bad Request!"
    });
  }
  console.error(err);
  return response.status(500).json({
    status: "Error",
    message: "Internal server error"
  });
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  exceptions
});
