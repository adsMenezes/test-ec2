FROM node:18.16.0

USER root

WORKDIR /app

COPY package.json .

RUN apt-get update && apt-get upgrade -y

RUN npm install -g pm2
RUN pm2 install pm2-logrotate
RUN npm install --production

COPY . .

RUN npx prisma generate

EXPOSE 80
