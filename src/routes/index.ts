import { Router } from 'express'

import { ensureAuthenticated } from '../middleware/ensureAuthenticated'

import { CreateSessionWithLinkedInController } from '../controllers/CreateSessionWithLinkedInController'

import { CreateChannelController } from '../controllers/CreateChannelController'
import { ProfileUserController } from '../controllers/ProfileUserController'
import { ShowChannelController } from '../controllers/ShowChannelController'

const routes = Router()

const createSessionWithLinkedInController = new CreateSessionWithLinkedInController()

const profileUserController = new ProfileUserController()
const createChannelController = new CreateChannelController()
const showChannelController = new ShowChannelController()

routes.get('/', (request, response) => {
  return response.json({
    message: 'Seja melhor que ontem!',
    author: 'Anderson Menezes <anderson@codrop.com.br>'
  })
})

routes.post('/sessions/linkedin', createSessionWithLinkedInController.handler)
routes.get('/me', ensureAuthenticated, profileUserController.handler)
routes.get('/channels/:channel_id', ensureAuthenticated, showChannelController.handler)
routes.post('/channels', ensureAuthenticated, createChannelController.handler)

export { routes }

