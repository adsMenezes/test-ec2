export interface IConnectChannelDTO {
  channel_id: string
  user_id: string
  rtc_offer: any
  rtc_answer: any
}
