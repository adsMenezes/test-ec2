import { NextFunction, Request, Response } from 'express'
import { AppError } from '../errors/AppError'


export function exceptions(
  err: Error,
  request: Request,
  response: Response,
  _: NextFunction
): Response {

  if (err instanceof AppError) {
    return response.status(err.statusCode).json({
      status: 'Error',
      message: err.message,
    })
  }

  if (err instanceof SyntaxError) {
    return response.status(400).json({
      status: 'Error',
      message: 'Bad Request!',
    })
  }

  console.error(err)
  return response.status(500).json({
    status: 'Error',
    message: 'Internal server error',
  })
}
