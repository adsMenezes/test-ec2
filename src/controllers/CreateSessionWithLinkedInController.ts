import { Request, Response } from "express";

import { CreateSessionWithLinkedInService } from "../services/CreateSessionWithLinkedInService";

export class CreateSessionWithLinkedInController {
  async handler(request: Request, response: Response): Promise<Response> {
    const { code } = request.body

    const createSessionWithLinkedIn = new CreateSessionWithLinkedInService()

    const { user, token } = await createSessionWithLinkedIn.execute(code)

    return response.json({ user, token })
  }
}
