import { Request, Response } from "express";

import { CreateChannelService } from "../services/CreateChannelService";

export class CreateChannelController {
  async handler(request: Request, response: Response): Promise<Response> {
    const createChannel = new CreateChannelService()

    const channel = await createChannel.execute()

    return response.json(channel)
  }
}
