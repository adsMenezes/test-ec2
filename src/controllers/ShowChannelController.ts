import { Request, Response } from "express";

import { ShowChannelService } from "../services/ShowChannelService";

export class ShowChannelController {
  async handler(request: Request, response: Response): Promise<Response> {
    const { user_id } = request
    const { channel_id } = request.params
    
    const showChannel = new ShowChannelService()

    await showChannel.execute({
      channel_id,
      user_id
    })

    return response.send()
  }
}
