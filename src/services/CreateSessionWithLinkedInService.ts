import axios from 'axios'
import fs from 'fs'
import { sign } from 'jsonwebtoken'
import crypto from 'node:crypto'
import path from 'path'

import { AppError } from '../errors/AppError'
import { prisma } from '../prisma'

interface ILocalized {
  localized: {
    [locale: string]: string
  }
}

interface ILinkedInTokenResponse {
  access_token: string
}

interface ILinkedInProfileResponse {
  id: string,
  firstName: ILocalized,
  lastName: ILocalized,
  profilePicture: {
    "displayImage~": {
      elements: {
        identifiers: {
          identifier: string
        }[]
      }[]
    }
  }
}

const uploadPath = path.resolve(__dirname, '..', 'temp')

export class CreateSessionWithLinkedInService {
  public async execute(linkedin_code: string) {
    try {      
      const tokenResponse = await axios.post<ILinkedInTokenResponse>(
        `https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=${linkedin_code}&redirect_uri=${process.env.LINKEDIN_REDIRECT_URI}&client_id=${process.env.LINKEDIN_CLIENT}&client_secret=${process.env.LINKEDIN_SECRET}`
      )

      const profileResponse = await axios.get<ILinkedInProfileResponse>(
        `https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~digitalmediaAsset:playableStreams))`,
        {
          headers: {
            Authorization: `Bearer ${tokenResponse.data.access_token}`
          }
        }
      )

      const { id, firstName, lastName } = profileResponse.data

      const name = `${Object.values(firstName.localized)[0]} ${Object.values(lastName.localized)[0]}` 
      let picture = profileResponse.data.profilePicture['displayImage~'].elements.pop()?.identifiers.pop()?.identifier

      
      let user

      user = await prisma.user.findUnique({
        where: { token: id }
      })

      if (!user) {
        if(picture) {
          try {
            const resposta = await axios.get(picture, {
              responseType: 'arraybuffer'
            });
  
            const buffer = Buffer.from(resposta.data, 'binary')
  
            const fileName = `${crypto.randomUUID()}.jpeg`
  
            fs.writeFileSync(`${uploadPath}/${fileName}`, buffer)
  
            picture = `${process.env.API_URL}/files/${fileName}`
          } catch (erro) {
            console.error('Ocorreu um erro ao baixar a imagem:', erro);
          }
        }
        
        user = await prisma.user.create({
          data: {
            name,
            picture,
            token: id,
          }
        })
      }

      const token = sign({}, `${process.env.JWT_SECRET}`, {
        subject: user.id,
        expiresIn: '1d',
      })

      return { user, token }
    } catch (err) {
      console.log(err)
      throw new AppError('Ocorreu um erro ao se autenticar com LinkedIn.')
    }
  }
}
