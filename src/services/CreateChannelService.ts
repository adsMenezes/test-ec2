import { prisma } from "../prisma";

export class CreateChannelService {
  async execute() {
    const channel = await prisma.channel.create({
      data: {}
    }) 
    
    return channel
  }
}
