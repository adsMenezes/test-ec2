import { prisma } from "../prisma";

interface IRequest {
  channel_id: string,
  user_id: string
}

export class ShowChannelService {
  async execute({ channel_id, user_id }: IRequest) {
    const channelUser = await prisma.channelUser.findUnique({
      where: {
        user_id: user_id,
      }
    })

    if (channelUser) {
      throw new Error("User already in a channel");
    }

    const channel = await prisma.channel.findUnique({
      where: { id: channel_id }
    })

    if (!channel) {
      throw new Error("Channel not found");
    }

    const customers = await prisma.channelUser.findMany({
      where: {
        channel_id,
        user_id: {
          notIn: user_id
        }
      }
    })

    if (customers.length > 2) {
      throw new Error("Channel is full");
    }
  }
}
