import 'dotenv/config'

import cors from 'cors'
import express from 'express'
import http from 'http'
import path from 'path'

import { verify } from 'jsonwebtoken'
import { Server } from 'socket.io'

import { exceptions } from './middleware/exceptions'
import { prisma } from './prisma'
import { routes } from './routes'

interface IPayload {
  sub: string
}

const app = express()
const server = http.createServer(app)

app.use(cors())
app.use('/files', express.static(path.resolve(__dirname, '..', 'temp')))
app.use(express.json())
app.use(routes)
app.use(exceptions)

const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

io.use((socket, next) => {
  const { token } = socket.handshake.auth;
  
  try {
    const { sub } = verify(token, `${process.env.JWT_SECRET}`) as IPayload
    
    socket.user_id = sub
    
    next()
  } catch {
    next(new Error("invalid"));
  }
});

io.on('connection', (socket) => {
  console.log('connected');

  socket.on('join', async ({ channel_id }) => {  
    const channel = await prisma.channel.findUnique({ 
      where: { 
        id: channel_id
      }
    })

    if(!channel) {
      return
    }

    await prisma.channelUser.create({
      data: {
        channel_id,
        user_id: socket.user_id,
        socket_id: socket.id,
      }
    })

    const customers = await prisma.channelUser.findMany({
      where: {
        channel_id
      },
      include: {
        user: true
      }
    })

    socket.join(channel_id)

    io.to(channel_id).emit('channel.set.customers', customers)

    if(customers.length >= 2) {
      io.in(socket.id).emit('peer.get.session.offer')
    }

    console.log('join -', channel_id)
  })

  socket.on('leave', async () => {
    const channel_id = Array.from(socket.rooms)[1];
    
    await prisma.channelUser.delete({ 
      where: { 
        user_id: socket.user_id 
      }
    })

    const customers = await prisma.channelUser.findMany({
      where: {
        channel_id,
      },
      include: {
        user: true
      }
    })    
    
    if(customers.length <= 0) {
      await prisma.channel.delete({
        where: {
          id: channel_id
        }
      })

      return 
    }

    socket.leave(channel_id)
    
    io.to(channel_id).emit('channel.set.customers', customers)
    io.to(channel_id).emit('peer.reset')

    console.log('leave -', channel_id)
  })

  socket.on('channel.isMicMuted', async (isMicMuted) => {
    const channel_id = Array.from(socket.rooms)[1];

    await prisma.channelUser.update({
      data: {
        is_microphone_active: isMicMuted
      }, 
      where: {
        user_id: socket.user_id
      }
    })

    const customers = await prisma.channelUser.findMany({
      where: {
        channel_id
      },
      include: {
        user: true
      }
    })

    io.to(channel_id).emit('channel.set.customers', customers)
  });
  
  socket.on('channel.session', async (session) => {
    const channel_id = Array.from(socket.rooms)[1];

    io.to(channel_id).except(socket.id).emit('peer.set.remote', session)
  });

  socket.on('channel.candidate', async (candidate) => {
    const channel_id = Array.from(socket.rooms)[1];

    io.to(channel_id).except(socket.id).emit('peer.set.candidate', candidate);
  });

  socket.on('talking', async (status) => {
    const channel_id = Array.from(socket.rooms)[1];

    io.to(channel_id).except(socket.id).emit('talking', status)
  })

  socket.on('disconnect', async () => {
    const channelUser = await prisma.channelUser.findUnique({
      where: {
        user_id: socket.user_id
      },
      include: {
        channel: true
      }
    })

    if(!channelUser) {
      console.log('disconnect')
      return
    }
    
    await prisma.channelUser.delete({ 
      where: { user_id: socket.user_id }
    })

    const customers = await prisma.channelUser.findMany({
      where: {
        channel_id: channelUser.channel.id
      },
      include: {
        user: true
      }
    })

    if(customers.length <= 0) {
      await prisma.channel.delete({
        where: {
          id: channelUser.channel.id
        }
      })

      return 
    }

    io.to(channelUser.channel.id).emit('channel.set.customers', customers)
    io.to(channelUser.channel.id).emit('peer.reset')

    console.log('channel disconnect -', channelUser.channel.id)
  })
});

server.listen(80, () => {
  console.log('listening on port 80')
})
